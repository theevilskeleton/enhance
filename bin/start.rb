require_relative '../lib/enhance'

QML.run do |app|
  QML.qt.application.organization = Enhance::ORG
  app.load_path "#{Enhance::LIBDIR}/qml/main.qml"
end