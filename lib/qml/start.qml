import QtQuick 2.15
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.3
import org.kde.kirigami 2.13 as Kirigami
import Enhance 0.1

Kirigami.Page {
    ColumnLayout {
        id: fileDialog1
        anchors.centerIn: parent
        spacing: Kirigami.Units.largeSpacing

        Controls.Button {
            id: openFiles
            Layout.alignment: Qt.AlignHCenter
            icon.name: "folder-open"
            icon.height: 64
            icon.width: 64
            onClicked: fileDialog.open()
        }

        Controls.Label {
            text: "Import Files to Begin"
        }
    }
}