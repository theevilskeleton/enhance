require 'qml'
require 'xdg'
require 'require_all'
require 'fileutils'
require 'logger'
require "active_support/all"

module Enhance
  BASEDIR = "#{__dir__}/../"
  LIBDIR = __dir__
  require_relative "enhance/version"
  require_relative "enhance/base"
end