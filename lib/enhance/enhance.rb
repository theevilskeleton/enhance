module Enhance
  class Enhance < Base
    include QML::Access
    register_to_qml

    property(:add) { [] }
    property(:remove) { [] }
    property(:model) { QML::ArrayModel.new(:title, :path) }
    property(:outputFolder) { '' }

    signal :showStart, []
    signal :showGrid, []
    signal :updateProgress, [:index, :total]

    def queue
      @queue ||= Queue.new
    end

    on_changed :add do
      on_first_file unless @first_file
      queue.add(add.to_a)
      reset_model
    end

    on_changed :remove do
      queue.remove(remove)
      reset_model
    end

    on_changed :outputFolder do
      Thread.new do
        process_batch(outputFolder)
      end
    end

    def process_batch(folder)
      queue.process(folder) do |index, total|
        QML.next_tick do
          reset_model
          updateProgress.emit(index, total)
        end
      end
      QML.next_tick do
        @first_file = false
        showStart.emit
      end
    end

    def on_first_file
      @first_file = true
      showGrid.emit
    end

    def reset_model
      model.clear
      queue.files.each { |file| add_to_model(file) }
    end

    def add_to_model(file)
      item = {
        title: file_title(file),
        path: file
      }
      model << item
    end

    def file_title(file)
      return file unless file.instance_of? String
      basename = File.basename(file)
      Queue::FILE_SUFFIXES.each do |suffix|
        next unless basename.end_with? suffix
        basename.delete_suffix!(suffix)
      end
      basename.parameterize.titleize
    end

    def quit
      QML.application.quit
    end

  end
end