module Enhance
  class Queue < Base
    FILE_SUFFIXES = [".png", ".jpg", ".webp"]

    def files
      @files ||= []
    end

    def file_names
      @file_names ||= files.map { |file| File.basename(file) }
    end

    def add(file)
      return add_string(file) if file.instance_of? String
      return add_array(file) if file.instance_of? Array
      false
    end

    def remove(file)
      if file.instance_of? String
        files.delete(file)
      elsif file.instance_of? Array
        file.each { |input| remove(input) }
      else
        false
      end
    end

    def process(output)
      output.delete_prefix! "file://"
      size = files.count
      count = 0
      logger.info("Starting batch of #{size}.")
      Upscaler.upscale(batch_to_temp, output) do |line|
        finished = get_finished(line)
        if finished.present?
          logger.info("Finished: #{finished}")
          count += 1
          index = file_names.index(finished)
          if index.present?
            files.delete_at(index)
          end
          if block_given?
            yield count, size
          end
        end
      end
      logger.info("Batch complete.")
      files = []
    end

    private

    def add_string(file)
      return false unless file.instance_of? String
      file.delete_prefix! "file://"
      return add_array(Dir["#{file}/**"]) if Dir.exist? file
      return false unless File.exist? file
      return false if files.include? file
      FILE_SUFFIXES.each do |suffix|
        next unless file.ends_with? suffix
        files << file
        return true
      end
      false
    end

    def get_finished(line)
      match = line.match(/^.+(?=->)/)
      return unless match.present?
      File.basename(match.to_s).strip
    end

    def batch_to_temp
      FileUtils.cp(files, temp, preserve: false)
      temp
    end

    def add_array(file_array)
      return false unless file_array.instance_of? Array
      file_array.each { |file| add(file) }
    end
  end
end