require "pty"
module Enhance
  class Upscaler < Base
    attr_accessor :input, :output

    def self.upscale(input, output)
      return false unless Dir.exist?(input) || Dir.exist?(output)
      upscaler = new(input, output)
      upscaler.process do |line|
        yield line
      end
    end

    def initialize(input, output)
      @input = input
      @output = output
      @buffer = []
    end

    def flag_params
      {
        input: input,
        output: output,
        scale: config.scale,
        model: config.model
      }
    end

    def command_flags
      flags = config.flags
      flag_params.each do |key, value|
        flags.sub!("$#{key}", value.to_s)
      end
      flags
    end
    
    def process
      upscaler = config.upscaler
      PTY.spawn(upscaler, *command_flags.split(" ")) do |stdout, stdin, pid|
        begin
          stdout.each { |line| yield line }
        rescue => e
          nil
        end
      end
    end
  end
end