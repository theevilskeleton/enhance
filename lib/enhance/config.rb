module Enhance
  class Config < Base
    DEFAULTS = {
      upscaler: "#{Enhance::BASEDIR}bin/realesrgan-ncnn-vulkan",
      model: "realesr-animevideov3",
      scale: 4,
      flags: "-v -i $input -o $output -s $scale -n $model",
      log: STDOUT
    }

    def initialize
      DEFAULTS.each do |key, value|
        self.class.attr_accessor key
        instance_variable_set("@#{key}", value)
      end
    end 
  end
end